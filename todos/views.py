from django.shortcuts import render, get_object_or_404

# from django.http import HttpResponse
from todos.models import TodoList, TodoItem


# Create List View --> for TodoList model -->
#  * View will get all instances of the TodoList model and
#  * put them in context of the template (for loop)
#    - Register view for a path
#    - Register todos paths with brain_two project
#    - Create a template for view (will include html file with for loop)


# Create your views here.
def todo_list_list(request):
    lists = TodoList.objects.all()
    context = {
        "todolist_all": lists,
    }
    return render(request, "todos/todolist.html", context)


def todo_list_detail(request, id):
    item = get_object_or_404(TodoItem, id=id)
    context = {
        "todolist_item_all": item,
    }
    return render(request, "todos/todoitem.html", context)
